<div class="{{ $folderClass }}">
    <a id="folder_{{ $folderId }}_link" class="folder-link" href="javascript:toggleFolder('{{ $folderId}}','{{$folderPath}}')">{{$folderName}}</a>
    <div id="folder_{{ $folderId }}" class="subfolder ml-2 hidden">Loading...</div>
</div>