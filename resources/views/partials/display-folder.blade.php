<div class="{{$folderClass}} display-folder">
    <a href="javascript:toggleFolder('{{ $folderId }}','{{ $folderPath }}')">{{ $folderName }}</a>
</div>