@extends('voyager-pages::layouts.default')
@section('meta_title', 'Documents')
@section('page_title', 'Documents')

@push('footer-scripts')
<script>
    window.fileData = {!! json_encode($folderData) !!};

    function displayRoot(){
        renderFolder('root', 'root', window.fileData.files);
        selectFolder('root');
    }

    function createTemplateHtml(templateId, replaceVars){
        let templates = document.getElementById('templates').getElementsByClassName(templateId)[0];
        let html = templates.outerHTML.replace(templateId,'');

        for(var i in replaceVars){
            html = html .replace(new RegExp(i,'g'), replaceVars[i]);
        }
        return html;
    }

    function createFolderHtml(folderId, folderPath, folderName){
        return createTemplateHtml('folderTemplate',{
            folderId: folderId,
            folderPath: folderPath,
            folderName: folderName
        });
    }

    function createDisplayFileHtml(folderId, folderPath, folderName){
        return createTemplateHtml('displayFileTemplate', {
            fileId: folderId,
            filePath: folderPath,
            fileName: folderName
        });
    }

    function createDisplayFolderHtml(folderId, folderPath, folderName){
        return createTemplateHtml('displayFolderTemplate', {
            folderId: folderId,
            folderPath: folderPath,
            folderName: folderName
        });
    }

    function renderFolder(folderId, path, data){
        let folderEl = document.getElementById('folder_' + folderId);
        let filesEl = document.getElementById('docList');

        let foldersHtml = '';
        let filesHtml = '';
        for(var i=0; i < data.length; i++){
            subFolderId = data[i].id;

            if(data[i].type=='dir'){
                foldersHtml += createFolderHtml(subFolderId, data[i].path, data[i].basename);

                filesHtml += createDisplayFolderHtml(subFolderId, data[i].path, data[i].basename);
            }
        }

        for(var i=0; i < data.length; i++){
            if(data[i].type !=='dir'){
                fileId = data[i].id;
                filesHtml += createDisplayFileHtml(fileId, data[i].path, data[i].basename);
            }
        }
        folderEl.innerHTML = foldersHtml;

        if(filesHtml == ''){
            filesHtml = "Hmm, this folder is empty";
        }
        document.getElementById('docList').innerHTML = filesHtml;
    }

    function getFolderData(folderPath, files){
        for(var i=0; i < files.length; i++){
            if(files[i].path == folderPath){
                return files[i];
            }else{
                if(files[i].children){
                    let data = getFolderData(folderPath, files[i].children);
                    if(data){
                        return data;
                    }
                }
            }
        }
        return false;
    }

    function addFolderData(files, data){
        for(var i=0; i < files.length; i++){
            if(files[i].path == data.path){
                files[i].children = data;
                renderFolder(files[i].id, files[i].path, data);
                return true;
            }else{
                if(files[i].children){
                    if(addFolderData(files[i].children, data)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    function displayFile(fileId, filePath){
        window.open(window.fileData.basePath + filePath);
    }

    function loadFolder(folderPath){
        document.getElementById('docList').innerHTML = '<div class=" flex justify-center items-center"><div class="animate-spin rounded-full h-32 w-32 border-b-2 border-gray-900"></div></div>';
        fetch(window.fileData.basePath + folderPath).then((result) => {
            result.json().then((data)=>{
                data.path = folderPath;
                addFolderData(window.fileData.files, data);
            });
        });
    }

    function toggleFolder(folderId, folderPath){
        folderId = folderId.replace(/\//g,"%2F");
        let folderEl = document.getElementById("folder_" + folderId);
        if(folderEl.classList.contains('hidden')){
            folderEl.classList.remove('hidden');
        }else{
            folderEl.classList.add('hidden');
        }

        let folderData = getFolderData(folderPath, window.fileData.files);
        if(folderData.children){
            renderFolder(folderData.id, folderPath, folderData.children);
        }else{
            loadFolder(folderPath);
        }
        selectFolder(folderId);
    }

    function selectFolder(folderId){
        var sels = document.getElementsByClassName('folders-menu')[0].getElementsByClassName('selected');
        for(var i=0; i < sels.length; i++){
            sels[i].classList.remove('selected');
        }

        document.getElementById('folder_' + folderId + '_link').classList.add('selected');
    }
</script>
@endpush

@section('content')
    <div class="alpha-iris-margin-element documents-page">
        <div class="md:flex documents-display">
            <div class="md:flex-1 md:w-2/5 w-full folders-menu">
                <div class="top-folder">
                    <a id="folder_root_link" class="folder-link selected" href="javascript:displayRoot()">Documents</a>
                </div>
                <div id="folder_root">
                    @foreach($docs as $doc)
                        @if($doc['type'] == 'dir')
                            @include('alpha-iris-documents::partials.menu-folder', [
                                'folderClass' => 'top-folder',
                                'folderId' => urlencode($doc['path']),
                                'folderPath' => $doc['path'],
                                'folderName' => $doc['basename']
                            ])
                        @endif
                    @endforeach
                </div>
            </div>
            <div id="docList" class="md:flex-auto md:w-3/5 w-full folders-display document-list">
                @foreach($docs as $doc)
                    @if($doc['type']=='dir')
                        @include('alpha-iris-documents::partials.display-folder', [
                            'folderClass' => '',
                            'folderId' => urlencode($doc['path']),
                            'folderPath' => $doc['path'],
                            'folderName' => $doc['basename']
                        ])
                    @else
                        @include('alpha-iris-documents::partials.display-file',[
                            'fileClass' => '',
                            'fileId' => urlencode($doc['path']),
                            'filePath' => $doc['path'],
                            'fileName' => $doc['basename']
                        ]);
                    @endif
                @endforeach                
            </div>
        </div>
    </div>

    <div id="templates" class="hidden">
        @include('alpha-iris-documents::partials.menu-folder', [
            'folderClass' => 'folderTemplate',
            'folderId' => 'folderId',
            'folderPath' => 'folderPath',
            'folderName' => 'folderName'
        ])

        @include('alpha-iris-documents::partials.display-folder',[
            'folderClass' => 'displayFolderTemplate',
            'folderId' => 'folderId',
            'folderPath' => 'folderPath',
            'folderName' => 'folderName'
        ])

        @include('alpha-iris-documents::partials.display-file',[
            'fileClass' => 'displayFileTemplate',
            'fileId' => 'fileId',
            'filePath' => 'filePath',
            'fileName' => 'fileName'
        ])        
    </div>
@endsection
