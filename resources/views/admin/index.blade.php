@extends('voyager::master')

@section('page_title', 'Documents')

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">

                <div class="admin-section-title">
                    <h1 class="page-title"><i class="voyager-images"></i>Documents</h1>
                </div>
                <div class="clear"></div>
                <form role="form" class="form-edit-add" action="{{ route('voyager.documents.settings') }}" method="POST" enctype="multipart/form-data">
                    <div class="form-group  col-md-12 ">                                   
                        <label class="control-label" for="name">Allow Access To</label>
                        <div>
                            @foreach($roles as $role)
                                <input type="checkbox" {{ in_array($role['id'],app('documents')->role_ids) ? 'checked' : '' }} name="role_id[]" value="{{ $role['id']}}">{{$role['label']}}<br/>
                            @endforeach
                            <input type="checkbox" {{ app('documents')->allow_guests ? 'checked' : '' }} name="allow_guests" value="true" />Allow Guests<br/>
                        </div>

                    </div>
                    {!!csrf_field()!!}
                    <button type="submit" class="btn btn-primary">Save Settings</button>
                </form>
                <div id="filemanager">
                    <media-manager
                        base-path="{{ setting('documents.storage-path', '/') }}"
                        files-url="{{ route('voyager.documents.files')}}"                        
                        new-folder-route="{{ route('voyager.documents.new_folder') }}" 
                        delete-route="{{ route('voyager.documents.delete') }}" 
                        move-route="{{ route('voyager.documents.move') }}"             
                        upload-route="{{ route('voyager.documents.upload') }}" 
                        rename-route="{{ route('voyager.documents.rename') }}" 
                        :allow-public-urls="false" 
                        :allow-permissions="true" 
                        :show-folders="true"
                        :allow-upload="true"
                        :allow-move="true"
                        :allow-delete="true"
                        :allow-create-folder="true"
                        :allow-rename="true"
                        :allow-crop="false"
                        permissions-url="{{ route('voyager.documents.permissions') }}" 
                        :details="{{ json_encode(['roles' => $roles, 'thumbnails' => config('voyager.media.thumbnails', []), 'watermark' => config('voyager.media.watermark', (object)[])]) }}"
                        ></media-manager>
                </div>
            </div><!-- .row -->
        </div><!-- .col-md-12 -->
    </div><!-- .page-content container-fluid -->
@stop

@section('javascript')
<script>
new Vue({
    el: '#filemanager'
});
</script>
<style>
    #filemanager{
        top: 0px;
    }
</style>
@endsection
