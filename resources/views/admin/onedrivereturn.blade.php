@extends('voyager::master')

@section('page_title', 'Documents')

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        
        @if($error)
            <h1>{{$error}}</h1>
            <xmp>
                {{ $errorDescription }}
            </xmp>
        @else
            <h1>You've successfully connected your OneDrive account</h1>
        @endif        
@stop

@section('javascript')

@endsection
