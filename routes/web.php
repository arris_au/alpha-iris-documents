<?php

Route::group(['middleware' => ['web']], function () {
    Route::group(['prefix' => 'admin', 'middleware' => 'admin.user', 'as' => 'voyager.'], function () {
        Route::post('documents/settings', '\AlphaIris\Documents\Http\Controllers\DocumentsController@settings')->name('documents.settings');
        Route::post('documents/files', '\AlphaIris\Documents\Http\Controllers\DocumentsController@files')->name('documents.files');
        Route::post('documents/permissions', '\AlphaIris\Documents\Http\Controllers\DocumentsController@permissions')->name('documents.permissions');
        Route::post('documents/new_folder', '\AlphaIris\Documents\Http\Controllers\DocumentsController@new_folder')->name('documents.new_folder');
        Route::post('documents/delete', '\AlphaIris\Documents\Http\Controllers\DocumentsController@delete')->name('documents.delete');
        Route::post('documents/move', '\AlphaIris\Documents\Http\Controllers\DocumentsController@move')->name('documents.move');
        Route::Post('documents/upload', '\AlphaIris\Documents\Http\Controllers\DocumentsController@upload')->name('documents.upload');
        Route::post('documents/rename', '\AlphaIris\Documents\Http\Controllers\DocumentsController@rename')->name('documents.rename');
        Route::get('documents', '\AlphaIris\Documents\Http\Controllers\DocumentsController@index')->name('documents.index');
        Route::get('onedrive/signin', '\AlphaIris\Documents\Http\Controllers\OneDriveController@signin')->name('onedrive.signin');
        Route::get('onedrive/callback', '\AlphaIris\Documents\Http\Controllers\OneDriveController@callback')->name('onedrive.callback');
        Route::get('onedrive/return', '\AlphaIris\Documents\Http\Controllers\OneDriveController@oneDriveReturn')->name('onedrive.return');
    });

    Route::get('documents', '\AlphaIris\Documents\Http\Controllers\Frontend\DocumentsController@index')->name('documents.index');
    Route::get('documents/{path}', '\AlphaIris\Documents\Http\Controllers\Frontend\DocumentsController@show')->name('documents.show')->where('path', '.*');
});
