<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Models\Setting;

class CreateDocumentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->string('filename');
            $table->string('path');
            $table->boolean('allow_guests')->default(false);
            $table->boolean('inherit_permissions')->default(false);
            $table->timestamps();
        });

        Schema::create('document_role', function (Blueprint $table) {
            $table->foreignId('document_id');
            $table->foreignId('role_id');

            $table->foreign('document_id')->references('id')->on('documents')->cascadeOnDelete();
            $table->foreign('role_id')->references('id')->on('roles')->cascadeOnDelete();

            $table->unique(['document_id', 'role_id']);
        });

        try {
            $setting = Setting::firstOrNew(['key' => 'documents.storage-path']);
            $setting->fill([
                'display_name' => 'Document Storage Path',
                'type' => 'text',
                'order' => 1,
                'group' => 'Documents',
                'value' => '/documents',
            ])->save();
        } catch (\Exception $e) {
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_role');
        Schema::dropIfExists('documents');
        try {
            $setting = Setting::firstOrFail(['key' => 'documents.storage-path']);
            $setting->delete();
        } catch (Exception $e) {
            {

        }
        }
    }
}
