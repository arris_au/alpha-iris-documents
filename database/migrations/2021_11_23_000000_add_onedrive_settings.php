<?php

use Illuminate\Database\Migrations\Migration;
use TCG\Voyager\Models\Setting;

class AddOnedriveSettings extends Migration
{
    protected $keys = [
        'azure-appId' => [
            'display_name' => 'OneDrive App Id',
        ],
        'azure-appSecret' => [
            'display_name' => 'OneDrive App Secret',
        ],
        'azure-tenantId' => [
            'display_name' => 'OneDrive Tenant Id',
        ],
        'azure-authToken' => [
            'display_name' => 'OneDrive Auth Token',
        ],
        'azure-refreshToken' => [
            'display_name' => 'OneDrive Refresh Token',
        ],
        'storage-disk' => [
            'display_name' => 'Storage Disk System',
        ],
    ];

    public function up()
    {
        $order = 2;
        foreach ($this->keys as $key => $data) {
            $setting = Setting::firstOrNew(['key' => 'documents.'.$key]);
            $setting->fill([
                'display_name' => $data['display_name'],
                'group' => 'Documents',
                'type' => 'text',
                'order' => $order,
            ])->save();
            $order++;
        }
    }

    public function down()
    {
        foreach ($this->keys as $key=>$data) {
            $setting = Setting::where('key', 'documents.'.$key)->first();
            if ($setting) {
                $setting->delete();
            }
        }
    }
}
