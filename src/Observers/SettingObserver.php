<?php

namespace AlphaIris\Documents\Observers;

use TCG\Voyager\Models\Setting;

class SettingObserver
{
    /**
     * Handle the Setting "created" event.
     *
     * @param  Setting  $user
     * @return void
     */
    public function created(Setting $setting)
    {
    }

    /**
     * Handle the Setting "updated" event.
     *
     * @param  Setting  $user
     * @return void
     */
    public function updated(Setting $setting)
    {
        switch ($setting->key) {
            case 'documents.azure-appId':
                app('config')->set('onedrive.appId', $setting->value);
                break;
            case 'documents.azure-appSecret':
                app('config')->set('onedrive.appSecret', $setting->value);
                break;
            case 'documents.azure-authToken':
                app('config')->set('onedrive.authToken', $setting->value);
                break;
            case 'documents.azure-refreshToken':
                app('config')->set('onedrive.refreshToken', $setting->value);
                break;
            case 'documents.azure-tenantId':
                app('config')->set('onedrive.tenantId', $setting->value);
                break;
        }
    }

    /**
     * Handle the Setting "deleted" event.
     *
     * @param  Setting $setting
     * @return void
     */
    public function deleted(Setting $setting)
    {
        //
    }

    /**
     * Handle the Setting "restored" event.
     *
     * @param Setting $setting
     * @return void
     */
    public function restored(Setting $setting)
    {
        //
    }

    /**
     * Handle the Setting "force deleted" event.
     *
     * @param  Setting $setting
     * @return void
     */
    public function forceDeleted(Setting $setting)
    {
        //
    }
}
