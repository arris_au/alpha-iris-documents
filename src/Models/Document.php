<?php

namespace AlphaIris\Documents\Models;

use AlphaIris\Core\Models\MembershipType;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\Role;

class Document extends Model
{
    protected $fillable = [
        'filename',
        'path',
        'allow_guests',
        'inherit_permissions',
    ];

    public static function getDocument($path)
    {
        $model = static::where('path', $path)->first();
        if (! $model) {
            $model = new static();
            $model->fill([
                'filename' => basename($path),
                'path' => $path,
                'inherit_permissions' => true,
                'allow_guests' => false,
            ])->save();
        }

        return $model;
    }

    public function getParentPathAttribute()
    {
        $path = preg_split("/\//", $this->path);
        array_pop($path);

        return implode('/', $path);
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_path', 'path');
    }

    public function roles()
    {
        if ($this->inherit_permissions) {
            if ($this->parent) {
                return $this->parent->roles();
            } else {
                return app('documents')->roles();
            }
        } else {
            return $this->belongsToMany(Role::class);
        }
    }
}
