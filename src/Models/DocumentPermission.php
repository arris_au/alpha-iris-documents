<?php

namespace AlphaIris\Documents\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentPermission extends Model
{
    protected $fillable = [
        'document_id',
        'role_id',
    ];
}
