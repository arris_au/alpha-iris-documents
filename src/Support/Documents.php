<?php

namespace AlphaIris\Documents\Support;

use AlphaIris\Documents\Models\Document;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasRelationships;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Plugin\ListWith;
use TCG\Voyager\Models\Role;

class Documents
{
    public $role_ids;
    public $allow_guests;
    protected $connection = null;

    public function __construct()
    {
        $roles = json_decode(setting('documents.allowed_roles'));
        if (! is_array($roles)) {
            $roles = [];
        }
        $this->role_ids = $roles;

        $this->allow_guests = setting('documents.allow_guests');
    }

    public function roles()
    {
        $query = $this->hasMany(Role::class, 'id', 'role_ids');
        $query->getQuery()->toBase()->setBindings([], 'where');
        $query->getQuery()->toBase()->wheres = [];
        $query->getQuery()->whereIn('id', $this->role_ids);

        return $query;
    }

    protected function hasMany($related, $foreignKey, $localKey)
    {
        $instance = $this->newRelatedInstance($related);
        $instance->{$localKey} = $this->$localKey;

        return $this->newHasMany(
            $instance->newQuery(), $instance, $instance->getTable().'.'.$foreignKey, $localKey
        );
    }

    protected function newHasMany(Builder $query, Model $parent, $foreignKey, $localKey)
    {
        return new HasMany($query, $parent, $foreignKey, $localKey);
    }

    /**
     * Create a new model instance for a related model.
     *
     * @param  string  $class
     * @return mixed
     */
    protected function newRelatedInstance($class)
    {
        return tap(new $class, function ($instance) {
            if (! $instance->getConnectionName()) {
                $instance->setConnection($this->connection);
            }
        });
    }

    public function allowed($path = null)
    {
        if ($path && $path !== '/') {
            $doc = Document::getDocument($path);
            $allow_guests = $doc->allow_guests;
            $roleIds = $doc->roles->pluck('id')->toArray();
        } else {
            $allow_guests = $this->allow_guests;
            $roleIds = $this->role_ids;
        }

        if (Auth::check()) {
            $matches = array_intersect(Auth::user()->roles->pluck('id')->toArray(), $roleIds);

            return count($matches) > 0;
        } else {
            return $allow_guests;
        }
    }

    public function getInfo($path = null)
    {
        $fs = $this->getFS();

        if ($path === null) {
            $path = setting('documents.storage-path').'/';
        }

        return $fs->get($path);
    }

    public function list($path = null)
    {
        if ($path === null) {
            $path = setting('documents.storage-path').'/';
        }
        $storage = $this->getFS();

        $storageItems = $storage->listWith(['mimetype'], $path);

        $newItems = [];
        foreach ($storageItems as $item) {
            if ($this->allowed($path.$item['path'])) {
                $item['id'] = urlencode($item['path']);
                $newItems[] = $item;
            }
        }

        return $newItems;
    }

    /**
     * Undocumented function.
     *
     * @return \Illuminate\Contracts\Filesystem\Filesystem
     */
    public function getFS()
    {
        $filesystem = setting('documents.storage-disk') ?: config('voyager.storage.disk');
        $storage = Storage::disk($filesystem)->addPlugin(new ListWith());

        return $storage;
    }
}
