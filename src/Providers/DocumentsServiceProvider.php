<?php

namespace AlphaIris\Documents\Providers;

use AlphaIris\Documents\Listeners\OneDriveTokensUpdatedListener;
use AlphaIris\Documents\Observers\SettingObserver;
use AlphaIris\Documents\Support\Documents;
use Arris\OneDrive\Events\OneDriveTokensUpdated;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use Illuminate\Support\Facades\App;
use TCG\Voyager\Models\Setting;

class DocumentsServiceProvider extends EventServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        OneDriveTokensUpdated::class => [
            OneDriveTokensUpdatedListener::class,
        ],
    ];

    public function boot()
    {
        Setting::observe(SettingObserver::class);

        app('config')->set('onedrive.errorCallbackRoute', 'voyager.onedrive.return');
        app('config')->set('onedrive.callbackRoute', 'voyager.onedrive.return');
        app('config')->set('onedrive.appId', setting('documents.azure-appId'));
        app('config')->set('onedrive.appSecret', setting('documents.azure-appSecret'));
        app('config')->set('onedrive.authToken', setting('documents.azure-authToken'));
        app('config')->set('onedrive.refreshToken', setting('documents.azure-refreshToken'));
        app('config')->set('onedrive.tenantId', setting('documents.azure-tenantId'));
        app('config')->set('onedrive.scopes', 'offline_access Files.ReadWrite.All');
        parent::boot();
    }

    public function register()
    {
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
        $this->loadViewsFrom([
            App::basePath().'/resources/views/vendor/alpha-iris-documents',
            __DIR__.'/../../resources/views/',
        ], 'alpha-iris-documents');
        parent::register();
        $this->app->bind('documents', function () {
            return new Documents();
        });
        AliasLoader::getInstance()->alias('Documents', \AlphaIris\Documents\Support\Facades\Documents::class);
    }
}
