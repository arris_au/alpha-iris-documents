<?php

namespace AlphaIris\Documents\Http\Controllers;

use Arris\OneDrive\Http\Controllers\OneDriveController as BaseOneDriveController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OneDriveController extends BaseOneDriveController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function signin()
    {
        $this->authorize('browse_media');

        return parent::signin();
    }

    public function callback(Request $request)
    {
        $this->authorize('browse_media');

        return parent::callback($request);
    }

    public function oneDriveReturn()
    {
        $error = Session::get('error');
        $errorDescription = Session::get('errorDetail');

        return view('alpha-iris-documents::admin.onedrivereturn', ['error' => $error, 'errorDescription' => $errorDescription]);
    }
}
