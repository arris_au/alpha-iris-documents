<?php

namespace AlphaIris\Documents\Http\Controllers\Frontend;

use AlphaIris\Core\Http\Controllers\AlphaIrisController;
use AlphaIris\Documents\Support\Facades\Documents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\File;
use League\Flysystem\Util\MimeType;

class DocumentsController extends AlphaIrisController
{
    public function index()
    {
        if (! app('documents')->allowed()) {
            abort(403);
        }

        $docs = app('documents')->list();
        $folderData = [
            'root' => setting('documents.storage-path').'/',
            'files' => $docs,
            'basePath' => route('documents.index').'/',
        ];

        return view('alpha-iris-documents::index', ['docs' => $docs, 'folderData' => $folderData]);
    }

    public function show(Request $request, $path)
    {
        $doc = app('documents');
        if (! $doc->allowed($path)) {
            abort(403);
        }

        $info = $doc->getInfo($path);
        if ($info instanceof File) {
            $mimeType = MimeType::detectByFilename($path);
            $size = $info->getSize();
            $dlHeaders = [
                'Cache-Control'         => 'must-revalidate, post-check=0, pre-check=0',
                'Content-Type'          => $mimeType,
                'Content-Length'        => $size,
                'Pragma'                => 'public',
            ];

            $viewableMimes = [
                'application/pdf',
                'image/jpg',
                'image/jpeg',
                'image/png',
                'image/gif',
            ];

            if (! in_array($mimeType, $viewableMimes)) {
                $dlHeaders['Content-Disposition'] = 'attachment; filename="'.basename($path).'"';
            }

            return response()->stream(function () use ($info) {
                $stream = $info->readStream();
                fpassthru($stream);
                if (is_resource($stream)) {
                    fclose($stream);
                }
            }, 200, $dlHeaders);
        } else {
            $docs = $doc->list($path);

            return $docs;
        }
    }
}
