<?php

namespace AlphaIris\Documents\Listeners;

use Arris\OneDrive\Events\OneDriveTokensUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use TCG\Voyager\Models\Setting;

class OneDriveTokensUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OneDriveTokensUpdated  $event
     * @return void
     */
    public function handle(OneDriveTokensUpdated $event)
    {
        $authSetting = Setting::where('key', 'documents.azure-authToken')->first();
        $refreshSetting = Setting::where('key', 'documents.azure-refreshToken')->first();

        $authSetting->value = $event->accessToken;
        $authSetting->save();

        $refreshSetting->value = $event->refreshToken;
        $refreshSetting->save();
    }
}
